package com.group.three;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Deque;



public class Run {

	public static void main(String[] args) {
		new Service();
	}

	public static class Constants {
		public static final float MAX_CREDIT_VALUE_ALLOWED = 100.00f;
	}
	
	public static class Service {
		// lifecycle var
		private boolean isRunning = true;
		
		private Deque<State> stateStack = new ArrayDeque<State>();
		private Database db = new Database();
		
		public Ticket ticket;
		
		// Main host of states
		public Service () {

			dummyData();
			
			System.out.println("### Exit program with input of 999 at any time ###");
			
			// Startup state is the welcome screen
			this.newState(new WelcomeState());
			// since everything is static, we set State.service to 'this' which allows all the lifecycle methods
			// available to all states through the state.service reference
			State.service = this;
			
			// main loop
			// continueally calls the state.action() method, which is actually abstract method that then calls
			// the implementing state's stateAction() method, getInput, and handle's input, giving the state
			// the opportunity to override how input is captured and handled.
			while (isRunning) {
				System.out.println("\n");	// two line break between state screens
				this.getState().action();
			}
			
		}
					
		private void dummyData () {
			for (int i = 0; i < 5; i++) {
				Ticket ticket = db.createTicket();
				if (Math.random() > 0.5) {
					DebitAction purchase = new PurchaseTicketAction();
					ticket.actions.add(purchase);
					ticket.actions.add(new FeeWaiverAction(purchase));
					if (Math.random() > 0.5) {
						ticket.actions.add(new PurchaseCreditAction((int)(Math.random() * 10) * 5));
					}
				}
			}
		}
		
		public void stopRunning () {
			this.isRunning = false;
		}
		
		// clears the state back to the welcome screen
		public void resetState () {
			this.stateStack.clear();
			this.newState(new WelcomeState());
		}

		public State getState () {
			return this.stateStack.peek();
		}
		
		//push state
		public void newState (State inState) {
			this.stateStack.push(inState);
		}
		
		//pop state
		protected State backState () {
			return this.stateStack.pop();
		}
	}
	
	/*
	 * Service is run by states
	 * State is a self contained state which we can use for global actions and call subclasses stateAction() method
	 * Also calls for input, handles input for global purposes, then passes unhandled input to the subclass
	 * handled() method. If not handled, prints out a default bad input line.
	 */
	public static abstract class State {

		public static Service service;
		private static BufferedReader reader = new BufferedReader(new InputStreamReader( System.in ) );
		
		public void action () {

			//System.out.print("DEBUG:: STATE: " + service.getState().getClass().getSimpleName() + " | ");
			//run implementing classes action method (tpyically displays options)
			this.stateAction();
			//display global options available for all screens
			if (!(service.stateStack.peek() instanceof WelcomeState)) {
				System.out.println("0. To cancel transaction");
			}
			
			//get user input
			UserInput input;
			try {
				//userinput obj will parse it as a number or throw an exception
				// stores numeric input as UserInput.value, stores orig input as UserInput. raw 
				input = new UserInput(this.getInput());
			} catch (IOException | NumberFormatException e) {
				System.out.println("Invalid input, please try again");
				return;
			}
			
			// pickup on globally handled input
			if (input.value == 999) {	//secret exit application input..
				 service.stopRunning();
				return;
				
			} else if (input.value == 99) {		//secret debug input
				service.db.printAll();
				System.out.println("\n");
				return;
				
			} else if (input.value == 0) {
				System.out.println("Transaction has been cancelled.");
				service.resetState();
				return;
			} 
			
			
			// if we get here and nothig has handled the input and returned, post unhandled msg and go again
			if(!handledInput(input)) {
				System.out.println("Sorry user input was not handled. Try again");
			}			
		}
		//basic string reader
		public static String getInput () throws IOException {
			System.out.print("INPUT> ");
			return reader.readLine();
		}
		
		public abstract void stateAction();
		public abstract boolean handledInput (UserInput inInput);
	}
	
	// welcome state
	public static class WelcomeState extends State {

		@Override
		public void stateAction () {
			System.out.println("Welcome to Cinco's PepPep services machine!\n=================================");
			System.out.println("Please select from the following options (enter number, then Enter key)");
			System.out.println("1. Purchase a new PepPep ticket (zero credit value)");
			System.out.println("2. Purchase credit for your PepPep ticket");
			System.out.println("3. Purcahse a travel pass");
			System.out.println("4. View ticket transaction history");
			System.out.println("9. Admin mode");
		}


		@Override
		public boolean handledInput(UserInput inInput) {	
			switch (inInput.value) {
			case 1:
				service.newState(new PurchaseTicketState());
				break;
			case 2: 
				// push retrieve tick state also - once ticket retrieved, state will be popped back to 
				// purchase credit with the appropriate Ticket in context (service.ticket)
				service.newState(new PurchaseCreditState());
				service.newState(new RetrieveTicketState());
				break;
			case 3: 
				// as above
				service.newState(new PurchasePassState());
				service.newState(new RetrieveTicketState());
				break;
			case 4: 
				service.newState(new TicketHistoryState());
				service.newState(new RetrieveTicketState());
				
				break;
			case 9: 
				service.newState(new AdminState());
				
				break;
			default:
				//wasn't any of them? wasn't handled then.
				return false;	
			}
			// hit one of the switch case's and skipped the default, so must've been handled.
			return true;
		}
	}
	
	
	public static class RetrieveTicketState extends State {

		@Override
		public void stateAction() {
			//ask for their ticket number
			System.out.println("Please enter your tickets five digit id number, eg 12345");
		}

		@Override
		public boolean handledInput(UserInput inInput) {
			// basic validation
			if (inInput.value < 10000 || inInput.value > 99999) {
				System.out.println("Sorry, your card number is invalid. Please try again.");
				return true;
			}
			
			//get their ticket from the database
			service.ticket = service.db.getTicketById(inInput.value);
			
			// wasnt found? staying in the same state so will loop around and ask again
			if (service.ticket == null) {
				System.out.println("Sorry, your card number was not found. Please try again.");
				return true;
			}
			
			// ticket was found, is stored in service.ticket in context, so pop this state
			// and let the previous state do what it wants with the found ticket
			System.out.println("Ticket number accepted");
			printTicketState(service.ticket);
			service.backState();
			return true;
		}

		private void printTicketState(Ticket ticket) {
			PurchasePassAction passAction = ticket.getLatestPass();
			if (passAction == null) {
				System.out.println("Note: There is no currently active pass for this ticket.");
				return;
			}
			LocalDateTime expires = getPassExpireDate(passAction);
			if (expires.isBefore(LocalDateTime.now())) {
				System.out.println("Note: Previous pass '" + passAction.description + "' has expired.");
			} else {
				
				LocalTime midnight = LocalTime.MIDNIGHT;
				LocalDate today = LocalDate.now(ZoneId.of("Australia/Sydney"));
				LocalDateTime todayMidnight = LocalDateTime.of(today, midnight).plusDays(1);
			
				if (expires.isAfter(todayMidnight)) {
					String expiresStr = todayMidnight.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
					System.out.println("Note: Previous pass '" + passAction.description + "' is valid until midnight of " + expiresStr);
				} else {
					String expiresStr = expires.format(DateTimeFormatter.ofPattern("hh:mm a dd/MM/yyyy "));
					System.out.println("Note: Previous pass '" + passAction.description + "' is valid until " + expiresStr);
				}
			}
		}
		
		private LocalDateTime getPassExpireDate (PurchasePassAction inAction) {
			long purchased = inAction.date.getTime();
			long expires = (purchased + inAction.duration);
			return Instant.ofEpochMilli(expires).atZone(ZoneId.systemDefault()).toLocalDateTime();
		}
		
		
	}
	
	public static class PurchaseTicketState extends State {

		@Override
		public void stateAction() {
			System.out.println("Please insert money value and press Enter.");
		}

		@Override
		public boolean handledInput(UserInput inInput) {
			// ticket action for use
			PurchaseTicketAction purchaseTicketAction = new PurchaseTicketAction();
			
			//not enough money put in
			if (inInput.value < purchaseTicketAction.value) {
				System.out.println("Sorry, $" + inInput.value + " was not enough for the $" + purchaseTicketAction.value + " ticket.");
				return true;
				
				// more than enough 
			} else if (inInput.value >= purchaseTicketAction.value) {
				// create a ticket, the DB will store the details
				service.ticket = service.db.createTicket();
				
				//add the purhcase ticket charge to it
				service.ticket.actions.add(purchaseTicketAction);
				//waive the purchase price - credit is an accumulation of all charges, when we start at 0 credit and place
				// -8 charge on the ticket, overall credit will be -8 - we can't purchase credit on the ticket
				// until after the ticket has been purchased, at which point it's -8 alrready? lets just waive that charge
				service.ticket.actions.add(new FeeWaiverAction(purchaseTicketAction));
				System.out.println("Thank you. Ticket " + service.ticket.id + " puchased");

				//convenience message for the user that puts > cost money
				if (inInput.value > purchaseTicketAction.value) {
					float change = inInput.value - purchaseTicketAction.value;
					System.out.printf("$ %.2f residule change dispursed.", change);
				}
				
				// transaction done, pop it back to welcome
				service.resetState();
				return true;
			}
			return false;
		}
		
	}
	
	public static class PurchaseCreditState extends State {


		@Override
		public void stateAction() {
			// incase someone goes back from the retrieve card state without having retrieved it - just send them back again
			if (service.ticket == null) {
				service.backState();
				return;
			}
			System.out.println("Please select amount of credit to purchase:");
			System.out.println("1. $5");
			System.out.println("2. $10");
			System.out.println("3. $20");
			System.out.println("4. $50");
			System.out.println("5. Specified amount");
			
		}

		@Override
		public boolean handledInput(UserInput inInput) {
			float moneyValue = 0;
			switch (inInput.value) {
				case 1:
					moneyValue = 5;
					break;
				case 2: 
					moneyValue = 10;
					break;
				case 3: moneyValue = 20;
					break;
				case 4: moneyValue = 50;
					break;
				case 5: 
					System.out.println("Please enter a specific about (must be a multiple of 5)");
					try {
						UserInput userInput = new UserInput(State.getInput());
						moneyValue = userInput.value;
						break;
					} catch (IOException e) {
						System.out.println("Failed to read user input, try again");
						return true;
					}
				default:
					System.out.println("Sorry, please select a valid amount");
					return true;	 //not enough, but it was handled..
			}
			 
			
			float credit = service.ticket.getCredit();
			
			if (moneyValue + credit > Constants.MAX_CREDIT_VALUE_ALLOWED) {
				System.out.println("Sorry, max credit allowed is $" + Constants.MAX_CREDIT_VALUE_ALLOWED + ", additional $" + moneyValue + " will amount to $" + (moneyValue + credit) + " credit on the Ticket.");
				return true;
			} else if (moneyValue % 5 != 0) { 
				System.out.println("The specified amount $" + moneyValue + " is not a multiple of 5 and is therefore invalid.");
				return true;
			} else {
				// put an action on the ticket record - this is a ref to the DB, so it'll appear there also
				service.ticket.actions.add(new PurchaseCreditAction(moneyValue));
				System.out.printf("Thank you, $%.2f credit has been added to ticket %d", moneyValue, service.ticket.id);
				service.backState();	
				return true;
			}
		}
		
	}
	
	public static class PurchasePassState extends State {

		@Override
		public void stateAction() {
			// incase someone goes back from the retrieve card state without having retrieved it - just send them back again
			if (service.ticket == null) {
				service.backState();
				return;
			}
			System.out.println("Please select which pass you'd like to purchase:");
			System.out.println("1. Two hours for Zone 1");
			System.out.println("2. Two hours for Zone 1 & 2");
			System.out.println("3. Whole day for Zone 1");
			System.out.println("4. Whole day for Zone 1 & 2");			
			
		}

		@Override
		public boolean handledInput(UserInput inInput) {

			switch(inInput.value) {
			case 1:
				purchasePass(new PurchasePassAction("Purchase two hours, Zone 1 pass", 3.5f, 2));
				break;
			case 2: 
				purchasePass(new PurchasePassAction("Purchase two hours, Zone 1 & 2 pass", 6, 2));
				break;
			case 3: 
				purchasePass(new PurchasePassAction("Purchase whole day, Zone 1 pass", 7, 24));
				break;
			case 4: 
				purchasePass(new PurchasePassAction("Purchase whole day, Zone 1 & 2 pass", 12, 24));
				break;
			default: 
				return false;
			}
			return true;
		}
		
		private void purchasePass (DebitAction inAction) {
			float ticketCredit = service.ticket.getCredit();
			if (inAction.value > ticketCredit) {
				System.out.printf("There is not enough credit on this Ticket ($%.2f) for transaction %s ($%.2f)\n", (float)ticketCredit, inAction.description, (float)inAction.value); 
				return;
			}
			service.ticket.actions.add(inAction);
			System.out.printf("%s has been purchased\n",  inAction.description);
			System.out.printf("$%.2f credit remaining on ticket", (float)service.ticket.getCredit());
			service.resetState();
		}
		
	}
	
	public static class TicketHistoryState extends State {

		// overriding action method of super to avoid super get input call
		@Override
		public void action() {
			// incase someone goes back from the retrieve card state without having retrieved it - just send them back again
			if (service.ticket == null) {
				service.backState();
				return;
			}
			
			service.db.printTicket(service.ticket);
			service.resetState();
			
			
		}

		@Override
		public boolean handledInput(UserInput inInput) {
			// nothing to handle	
			return true;
		}

		@Override
		public void stateAction() {
			// TODO Auto-generated method stub
			
		}
			
	}
	
	public static class AdminState extends State {

		@Override
		public void stateAction() {
			System.out.println("******************************************");
			System.out.println(" Cinco's PepPep services machine - ADMIN MODE");
			System.out.println("******************************************");
			System.out.println("Please select from the following options (enter number, then Enter key)");
			System.out.println("1. Display all transactions stored in database");
			System.out.println("2. Display all ticket purchase transactions, sorted by ticket id");
			System.out.println("3. Display all ticket purchase transactions, sorted by purchase date");
			
		}

		
		@Override
		public boolean handledInput(UserInput inInput) {

			switch(inInput.value) {
			case 1:
				service.db.printAll();
				break;
			case 2: 
				printPassesById();
				break;
			case 3: 
				printPassesByDate();
				break;
			default: 
				return false;
			}
			return true;
		}
		
		public void printPassesByDate () {
			ArrayList<PrintableTicketItem> passes = getAllPasses();
			passes.sort(new Comparator<PrintableTicketItem>() {

				@Override
				public int compare(PrintableTicketItem thisItem, PrintableTicketItem toItem) {
					if (thisItem.date.before(toItem.date)) return -1; return 1;
				}
			});
			
			System.out.println("Purchased passes sorted by date\n------------------------------------------");
			
			for (PrintableTicketItem t: passes) {
				System.out.print("TicketId: " + t.id + ", ");
				System.out.println(t.action.toString());
			}
			
		}
		
		public void printPassesById () {
			ArrayList<PrintableTicketItem> passes = getAllPasses();
			passes.sort(new Comparator<PrintableTicketItem>() {

				@Override
				public int compare(PrintableTicketItem thisItem, PrintableTicketItem toItem) {
					if (thisItem.id <= toItem.id) return -1; return 1;
				}
			});


			System.out.println("Purchased passes sorted by ticket id\n------------------------------------------");
			for (PrintableTicketItem t: passes) {
				System.out.print("TicketId: " + t.id + ", ");
				System.out.println(t.action.toString());
			}
		}
		
		public ArrayList<PrintableTicketItem> getAllPasses () {
			ArrayList<PrintableTicketItem> actions = new ArrayList<PrintableTicketItem>();
			
			for (Ticket t: service.db.getTickets()) {
				for (Action a: t.actions) {
					if (a instanceof PurchasePassAction) {
						actions.add(new PrintableTicketItem(t.id, a.date, a));
					}
				}
			}
			
			return actions;
		}
		
		private class PrintableTicketItem {
			public long id;
			public Date date;
			public Action action;
			public PrintableTicketItem (long inId, Date inDate, Action inAction) {
				this.id = inId;
				this.date = inDate;
				this.action = inAction;
			}
		}
	}
	
	public static class Ticket {
		private static long ticketIdCounter = 10000;
		public long id = Ticket.ticketIdCounter++;
		public ArrayList<Action> actions = new ArrayList<Action>();	
		
		public float getCredit () {
			float val = 0;
			for (Action t: actions) {
				if (t instanceof DebitAction) {
					val -= t.value;
				} else {	// instance of CreditAction, so add to credit value
					val += t.value;
				}
			}
			return val;
		}
		
		public PurchasePassAction getLatestPass () {
			Action passAction = null;
			for (Action a: actions) {
				if (a instanceof PurchasePassAction) {
					if (passAction == null) {
						passAction = a;
						continue;
					}
					if (a.date.after(passAction.date)) {
						passAction = a;
					}
				}
			}
			return (PurchasePassAction) passAction;
		}
		
		public String toString () {
			StringBuilder sb = new StringBuilder();
			sb.append("TicketId: " + this.id + ", ");
			sb.append("credit: $ " + this.getCredit());
			return sb.toString();
		}
	}
		
	public static abstract class Action {
		private static long transactionIdCounter = 20000;
		public Date date = new Date();
		public long id = transactionIdCounter++;
		public float value;
		public String description;
		
		public Action (String inDescription, float inValue) {
			this.description = inDescription;
			this.value = inValue;
		}
		
		public String toString () {
			StringBuilder sb = new StringBuilder();
			sb.append("id: " + id + ", ");
			sb.append("date: " + date + ", ");
			sb.append("value: $" + value + ", ");
			sb.append("desc: " + description + "");
			return sb.toString();
		}
	}
	
		
	public static abstract class CreditAction extends Action { 
		public CreditAction(String inDescription, float inValue) {
			super(inDescription, inValue);
		}		
	}
	
	public static abstract class DebitAction extends Action {
		public DebitAction(String inDescription, float inValue) {
			super(inDescription, inValue);
		}
	}

	public static class FeeWaiverAction extends CreditAction {
		public FeeWaiverAction(DebitAction inDebit) {
			super("Fee Waived: " + inDebit.description, inDebit.value);
		}
	}

	public static class PurchaseCreditAction extends CreditAction {
		public PurchaseCreditAction(float moneyValue) {
			super("Additional credit purchase", moneyValue);
		}
	}

	public static class PurchaseTicketAction extends DebitAction {
		public PurchaseTicketAction() {
			super("PepPep ticket purchase", 8);
		}
	}

	public static class PurchasePassAction extends DebitAction {
		public long duration = 0;
		public PurchasePassAction (String inDescription, float inValue, long inDurationInHours) {
			super(inDescription, inValue);
			this.duration = inDurationInHours * 60 * 60 * 1000;	// convert to milliseconds
		}
	}
	
	
	public static class UserInput {
		public String raw;
		public int value;
		
		public UserInput (String inStr) {
			this.raw = inStr;
			this.value = Integer.parseInt(inStr);	//throws number format exception
		}		
	}
	
	
	public static class Database {


		private ArrayList<Ticket> db = new ArrayList<Ticket>();

		public ArrayList<Ticket> getTickets () {
			return db;
		}
		
		public Ticket getTicketById (long inId) {
			for (Ticket ticket: db) { if (ticket.id == inId) return ticket; }
			return null;
		}
		
		public Ticket createTicket () {
			Ticket ticket = new Ticket();
			db.add(ticket);
			return ticket;
		}
		
		public void printTicket (Ticket inTicket) {
			System.out.println(inTicket.toString());
			if (inTicket.actions.size() == 0) { 
				System.out.println("\t --- NO TRANSACTION RECORDED ---");
			}
			for (Action ts: inTicket.actions) {
				System.out.println("\t" + ts.toString());
			}
		
		}
		
		
		public void printAll () {
			for (Ticket t: db) {
				printTicket(t);
			}
		}
	
	}

}
